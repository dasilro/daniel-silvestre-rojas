﻿using Learning.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Learning.Domain.Interfaces
{
    public interface IBaseRepository<T>  where T : BaseEntity
    {        
        public IUnitOfWork UnitOfWork { get; }
        Task<int> Count(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null);
        Task<ICollection<T>> Get(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null,
                                 string orderByPropertyName = "CreateDate", bool ascendingOrderBy = true);
        Task<IPagedResult<T>> GetPaged(ICollection<Expression<Func<T, bool>>> filters, int limit = 0, int offset = 0, string orderByPropertyName = "CreateDate",
                                  bool ascendingOrderBy = true, bool includeTotalCount = true, ICollection<string> includes = null);
        T Create(T instance);
        T Update(T instance);
        void Delete(T instance);        
    }
}
