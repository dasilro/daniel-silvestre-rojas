﻿using Learning.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Learning.Domain.Interfaces
{
    public interface IGuidEntityRepository<T> : IBaseRepository<T> where T : BaseGuidEntity
    {
        Task<T> GetById(Guid Id);
        Task Delete(Guid id);
    }
}
