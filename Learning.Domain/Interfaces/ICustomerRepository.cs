﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface ICustomerRepository: IGuidEntityRepository<Customer>
    {
    }
}
