﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace Learning.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        public DbContext DbContext { get; }
        Task<IDbContextTransaction> BeginTransaction();
        Task CommitTransaction(IDbContextTransaction transaction);
        Task RollbackTransaction(IDbContextTransaction transaction);

        Task SaveChanges();
        void CancelChanges();
    }
}
