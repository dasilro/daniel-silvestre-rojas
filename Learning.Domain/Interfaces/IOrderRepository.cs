﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface IOrderRepository: IGuidEntityRepository<Order>
    {
    }
}
