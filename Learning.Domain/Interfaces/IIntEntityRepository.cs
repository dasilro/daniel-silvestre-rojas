﻿using Learning.Domain.Entities;
using System.Threading.Tasks;

namespace Learning.Domain.Interfaces
{
    public interface IIntEntityRepository<T> : IBaseRepository<T> where T : BaseIntEntity
    {
        Task<T> GetById(int Id);
        Task Delete(int id);
    }
}
