﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface IOrderLineRepository: IGuidEntityRepository<OrderLine>
    {
    }
}
