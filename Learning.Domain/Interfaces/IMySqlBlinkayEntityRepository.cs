﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface IMySqlBlinkayEntityRepository: IIntEntityRepository<BlinkayEntity>
    {        
    }
}
