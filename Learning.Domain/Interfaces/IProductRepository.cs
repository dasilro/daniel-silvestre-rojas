﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface IProductRepository: IGuidEntityRepository<Product>
    {
    }
}
