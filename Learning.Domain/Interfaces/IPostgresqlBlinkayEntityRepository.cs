﻿using Learning.Domain.Entities;

namespace Learning.Domain.Interfaces
{
    public interface IPostgresqlBlinkayEntityRepository : IIntEntityRepository<BlinkayEntity>
    {
    }
}
