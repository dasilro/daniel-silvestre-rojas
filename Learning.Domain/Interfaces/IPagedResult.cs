﻿using Learning.Domain.Entities;
using System.Collections.Generic;

namespace Learning.Domain.Interfaces
{
    public interface IPagedResult<T>
    {
        public List<T> Items { get; set; }
        public int TotalCount { get; set; }
    }
}
