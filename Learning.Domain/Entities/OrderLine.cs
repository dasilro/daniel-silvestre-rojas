﻿using System;

namespace Learning.Domain.Entities
{
    public class OrderLine : BaseGuidEntity
    {
        public Guid OrderId { get; set; }
        public virtual Order Order {get; set;}
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
        public decimal ProductQuantity { get; set; }
        public decimal UnitaryPrice { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal WithoutTaxesTotal { get; set; }
        public decimal TaxPercentage { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
    }
}
