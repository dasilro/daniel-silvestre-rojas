﻿using System;
using System.Collections.Generic;

namespace Learning.Domain.Entities
{
    public class Order : BaseGuidEntity
    {
        public string SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public Guid CustomerId { get; set; }
        public virtual Customer Customer {get; set;}
        public decimal WithoutTaxesTotal { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public virtual ICollection<OrderLine> OrderLines { get; set; }
    }
}
