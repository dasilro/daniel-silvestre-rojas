﻿using System.Collections.Generic;

namespace Learning.Domain.Entities
{
    public class Customer: BaseGuidEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
