﻿using System;

namespace Learning.Domain.Entities
{
    public class BaseIntEntity: BaseEntity
    {
        public int Id { get; set; }
    }
}
