﻿using System;

namespace Learning.Domain.Entities
{
    public class BaseEntity
    {        
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
