﻿
using Microsoft.AspNetCore.Identity;

namespace Learning.Domain.Entities
{
    public class ApplicationUser: IdentityUser
    {
    }
}
