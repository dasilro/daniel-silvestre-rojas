﻿using System;

namespace Learning.Domain.Entities
{
    public class BaseGuidEntity: BaseEntity
    {
        public Guid Id { get; set; }
    }
}
