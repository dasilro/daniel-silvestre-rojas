﻿using System.Collections.Generic;

namespace Learning.Domain.Entities
{
    public class Product : BaseGuidEntity
    {
        public string Name { get; set; }
        public decimal UnitaryPrice { get; set; }
        public virtual ICollection<OrderLine> OrderLines { get; set; }
    }
}
