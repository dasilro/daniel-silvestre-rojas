using Learning.Application.Services;
using Learning.Common;
using Learning.Domain.Entities;
using Learning.Persistence.DbContexts;
using Learning.Persistence.Repositories;
using Learning.Persistence.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Learning
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();


            services.AddDbContext<LearningDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("LearningDbContext")));

            services.AddDbContext<MySqlBlinkayDbContext>(options =>
                    options.UseMySQL(Configuration.GetConnectionString("MySqlBlinkayDbContext")));

            services.AddDbContext<PostgresqlBlinkayDbContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("PostgresqlBlinkayDbContext")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<LearningDbContext>();

            services.AddAuthorization(auth =>
            {
                auth.FallbackPolicy =
                    new AuthorizationPolicyBuilder()
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser().Build();
            });

            services
                .AddAuthentication(options => options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                            {
                                context.Response.Headers.Add("Token-Expired", "true");
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Learning", Version = "v1" });
                c.AddSecurityDefinition(
                    "token",
                    new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.Http,
                        BearerFormat = "JWT",
                        Scheme = "Bearer",
                        In = ParameterLocation.Header,
                        Name = HeaderNames.Authorization
                    }
                );
                c.AddSecurityRequirement(
                    new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "token"
                                },
                            },
                            Array.Empty<string>()
                        }
                    }
                );
            });

            ConfigureIoC(services);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }

        private void ConfigureIoC(IServiceCollection services)
        {
            services.AddTransient<MySqlBlinkayDbContext>();
            services.AddTransient<PostgresqlBlinkayDbContext>();

            services.AddScoped<ILearningUnitOfWork, LearningUnitOfWork>();
            services.AddTransient<IMySqlBlinkayUnitOfWork, MySqlBlinkayUnitOfWork>();
            services.AddTransient<IPostgresqlBlinkayUnitOfWork, PostgresqlBlinkayUnitOfWork>();



            // Register all repositories and services from assemblies.
            List<Assembly> assemblies = new List<Assembly>();
            assemblies.Add(Assembly.GetAssembly(typeof(Customer)));             // Domain assembly.
            assemblies.Add(Assembly.GetAssembly(typeof(CustomerRepository)));   // Persitence assembly.
            assemblies.Add(Assembly.GetAssembly(typeof(CustomerService)));      // Application assembly.

            var typesFromAssemblies = assemblies.SelectMany(a => (a.DefinedTypes
                                                                   .Where(x => x.GetInterfaces().Any()
                                                                               && (x.IsClass)
                                                                               && (x.FullName.EndsWith("Service") ||
                                                                                   x.FullName.EndsWith("Repository")))));
            foreach (var type in typesFromAssemblies)
            {
                foreach (Type interf in type.GetInterfaces())
                {
                    Console.WriteLine($"Interface {interf.Name} Type {type.Name}");
                    if (interf.Name.Contains("Blinkay") && interf.Name.Contains("Repository"))
                    {
                        services.Add(new ServiceDescriptor(interf, type, ServiceLifetime.Transient));
                    }
                    else
                    {
                        services.Add(new ServiceDescriptor(interf, type, ServiceLifetime.Scoped));
                    }

                }
            }
        }


        private void SetupJWTServices(IServiceCollection services)
        {
            string key = "my_secret_key_12345"; //this should be same which is used while creating token      
            var issuer = "http://mysite.com";  //this should be same which is used while creating token  

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
          .AddJwtBearer(options =>
          {
              options.TokenValidationParameters = new TokenValidationParameters
              {
                  ValidateIssuer = true,
                  ValidateAudience = true,
                  ValidateIssuerSigningKey = true,
                  ValidIssuer = issuer,
                  ValidAudience = issuer,
                  IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
              };

              options.Events = new JwtBearerEvents
              {
                  OnAuthenticationFailed = context =>
                  {
                      if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                      {
                          context.Response.Headers.Add("Token-Expired", "true");
                      }
                      return Task.CompletedTask;
                  }
              };
          });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IWebHostEnvironment env,
                              LearningDbContext learningDbContext,
                              MySqlBlinkayDbContext mySqlBlinkayDbContext,
                              PostgresqlBlinkayDbContext postgresqlBlinkayDbContext)
        {
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Learning v1"));


            //}


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseExceptionHandler(a => a.Run(async context =>
            {
                context.Response.StatusCode = 500;
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;

                if (exception is LearningException)
                {
                    context.Response.StatusCode = 400;
                }

                await context.Response.WriteAsJsonAsync(new { error = exception.Message });
            }));


            learningDbContext.Database.EnsureCreated();
            mySqlBlinkayDbContext.Database.EnsureCreated();
            postgresqlBlinkayDbContext.Database.EnsureCreated();
        }
    }
}
