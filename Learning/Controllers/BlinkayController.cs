﻿using Learning.Application.Services.Interfaces;
using Learning.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Learning.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlinkayController : ControllerBase
    {
        private readonly IBlinkayService blinkayService;

        public BlinkayController(IBlinkayService blinkayService)
        {
            this.blinkayService = blinkayService;
        }

        [HttpPost("MySQLInsertion")]
        public async Task<IActionResult> MySQLInsertion(int numRegistries, int numThreads) 
        {
            try
            {
                return Ok(await blinkayService.MySQLInsertion(numRegistries, numThreads));
            }
            catch (LearningException ex) 
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("MySQLSelectPlusUpdate")]
        public async Task<IActionResult> MySQLSelectPlusUpdate(int numRegistries, int numThreads)
        {
            try
            {
                return Ok(await blinkayService.MySQLSelectPlusUpdate(numRegistries, numThreads));
            }
            catch (LearningException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("MySQLSelectPlusUpdatePlusInsertion")]
        public async Task<IActionResult> MySQLSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads)
        {
            try
            {
                return Ok(await blinkayService.MySQLSelectPlusUpdatePlusInsertion(numRegistries, numThreads));
            }
            catch (LearningException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("PGInsertion")]
        public async Task<IActionResult> PostgresqlInsertion(int numRegistries, int numThreads)
        {
            try
            {
                return Ok(await blinkayService.PGInsertion(numRegistries, numThreads));
            }
            catch (LearningException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("PGSelectPlusUpdate")]
        public async Task<IActionResult> PostgresqlSelectPlusUpdate(int numRegistries, int numThreads)
        {
            try
            {
                return Ok(await blinkayService.PGSelectPlusUpdate(numRegistries, numThreads));
            }
            catch (LearningException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("PGSelectPlusUpdatePlusInsertion")]
        public async Task<IActionResult> PostgresqlSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads)
        {
            try
            {
                return Ok(await blinkayService.PGSelectPlusUpdatePlusInsertion(numRegistries, numThreads));
            }
            catch (LearningException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
