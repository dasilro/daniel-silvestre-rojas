﻿using Learning.Application.Dtos.Account;
using Learning.Application.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Learning.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService, IConfiguration configuration) 
        { 
            this.accountService = accountService;
        }

        [HttpPost]
        [Route("register")]
        public async Task<ActionResult> Register(RegisterDto registerDto) 
        {
            var identityResult = await accountService.Register(registerDto);

            if (identityResult.Succeeded)
            {
                return Ok();
            }
            else 
            {
                return BadRequest(identityResult.Errors);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getTokenJwt")]
        public async Task<ActionResult> GetTokenJwt(string email, string password)
        {
            var tokenJwt = await accountService.GetTokenJwt(email, password);
            return Ok(tokenJwt);
        }
    }
}
