﻿using Learning.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Learning.Persistence.DbContexts
{
    public class PostgresqlBlinkayDbContext : DbContext
    {
        public PostgresqlBlinkayDbContext(DbContextOptions<PostgresqlBlinkayDbContext> options)
            : base(options)
        {
        }

        public PostgresqlBlinkayDbContext DbContext { get { return this; } }

        public DbSet<BlinkayEntity> BlinkayEntities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<BlinkayEntity>().ToTable("BlinkayEntities");
            modelBuilder.Entity<BlinkayEntity>().HasKey("Id");

            modelBuilder.Entity<BlinkayEntity>().Property(x => x.Id).HasIdentityOptions(1, 1);            
        }
    }
}
