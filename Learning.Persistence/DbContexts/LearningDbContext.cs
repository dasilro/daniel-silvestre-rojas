﻿using Learning.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace Learning.Persistence.DbContexts
{
    public class LearningDbContext : IdentityDbContext<IdentityUser>
    {
        public LearningDbContext (DbContextOptions<LearningDbContext> options)
            : base(options)
        {
        }

        public LearningDbContext DbContext { get { return this; } }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Customer>().HasKey("Id");

            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<Product>().HasKey("Id");

            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<Order>().HasKey("Id");
            modelBuilder.Entity<Order>().HasOne<Customer>(x => x.Customer).WithMany(x => x.Orders).HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<OrderLine>().ToTable("OrderLines");
            modelBuilder.Entity<OrderLine>().HasKey("Id");
            modelBuilder.Entity<OrderLine>().HasOne<Order>(x => x.Order).WithMany(x => x.OrderLines).HasForeignKey(x => x.OrderId);
            modelBuilder.Entity<OrderLine>().HasOne<Product>(x => x.Product).WithMany(x => x.OrderLines).HasForeignKey(x => x.ProductId);
        }
    }
}
