﻿using Learning.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Learning.Persistence.DbContexts
{
    public class MySqlBlinkayDbContext : DbContext
    {
        public MySqlBlinkayDbContext(DbContextOptions<MySqlBlinkayDbContext> options)
            : base(options)
        {
        }

        public MySqlBlinkayDbContext DbContext { get { return this; } }

        public DbSet<BlinkayEntity> BlinkayEntities { get; set; }        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlinkayEntity>().ToTable("BlinkayEntities");
            modelBuilder.Entity<BlinkayEntity>().HasKey("Id");

            modelBuilder.Entity<BlinkayEntity>().Property(x => x.Id).HasIdentityOptions(1, 1);            
        }        
    }
}
