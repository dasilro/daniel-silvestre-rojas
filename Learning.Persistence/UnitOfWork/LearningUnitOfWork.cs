﻿using Learning.Persistence.DbContexts;

namespace Learning.Persistence.UnitOfWork
{
    public class LearningUnitOfWork : UnitOfWork, ILearningUnitOfWork
    {        
        public LearningUnitOfWork(LearningDbContext dbContext) : base (dbContext)
        {            
        }
    }
}
