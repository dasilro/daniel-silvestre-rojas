﻿using Learning.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace Learning.Persistence.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        public DbContext DbContext { get; }

        public UnitOfWork(DbContext dbContext) 
        { 
            this.DbContext = dbContext;
        }

        public async Task SaveChanges()
        {
            await DbContext.SaveChangesAsync();
        }

        public void CancelChanges()
        {
            DbContext.ChangeTracker.Clear();
        }

        public async Task<IDbContextTransaction> BeginTransaction()
        {
            return await DbContext.Database.BeginTransactionAsync();
        }

        public async Task CommitTransaction(IDbContextTransaction transaction)
        {
            await transaction.CommitAsync();
        }

        public async Task RollbackTransaction(IDbContextTransaction transaction)
        {
            await transaction.RollbackAsync();
        }
    }
}
