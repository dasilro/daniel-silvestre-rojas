﻿using Learning.Domain.Interfaces;

namespace Learning.Persistence.UnitOfWork
{
    public interface ILearningUnitOfWork: IUnitOfWork
    {
    }
}
