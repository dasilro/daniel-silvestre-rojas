﻿using Learning.Persistence.DbContexts;

namespace Learning.Persistence.UnitOfWork
{
    public class MySqlBlinkayUnitOfWork : UnitOfWork, IMySqlBlinkayUnitOfWork
    {        
        public MySqlBlinkayUnitOfWork(MySqlBlinkayDbContext dbContext) : base (dbContext)
        {            
        }
    }
}
