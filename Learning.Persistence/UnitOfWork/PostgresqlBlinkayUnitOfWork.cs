﻿using Learning.Persistence.DbContexts;

namespace Learning.Persistence.UnitOfWork
{
    public class PostgresqlBlinkayUnitOfWork : UnitOfWork, IPostgresqlBlinkayUnitOfWork
    {     
        public PostgresqlBlinkayUnitOfWork(PostgresqlBlinkayDbContext dbContext) : base (dbContext) 
        {
        }
    }
}
