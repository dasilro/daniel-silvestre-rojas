﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class OrderRepository: GuidEntityRepository<Order>, IOrderRepository
    {
        public OrderRepository(ILearningUnitOfWork unitOfWork): base(unitOfWork)
        {            
        }
    }
}
