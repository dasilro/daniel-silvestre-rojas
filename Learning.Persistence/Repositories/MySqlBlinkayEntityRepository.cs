﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class MySqlBlinkayEntityRepository : IntEntityRepository<BlinkayEntity>, IMySqlBlinkayEntityRepository
    {
        public MySqlBlinkayEntityRepository(IMySqlBlinkayUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
