﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;
using System;
using System.Threading.Tasks;

namespace Learning.Persistence.Repositories
{
    public class IntEntityRepository<T> : BaseRepository<T>, IIntEntityRepository<T> where T : BaseIntEntity
    {
        public IntEntityRepository(IUnitOfWork unitOfWork): base(unitOfWork)
        {
        }

        public async Task Delete(int id)
        {
            Set.Remove(await GetById(id));
        }

        public async Task<T> GetById(int Id)
        {
            return await Set.FindAsync(Id);
        }
    }
}
