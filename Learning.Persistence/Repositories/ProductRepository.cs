﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class ProductRepository: GuidEntityRepository<Product>, IProductRepository
    {
        public ProductRepository(ILearningUnitOfWork unitOfWork): base(unitOfWork)
        {            
        }
    }
}
