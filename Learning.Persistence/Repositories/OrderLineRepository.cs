﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class OrderLineRepository: GuidEntityRepository<OrderLine>, IOrderLineRepository
    {
        public OrderLineRepository(ILearningUnitOfWork unitOfWork): base(unitOfWork)
        {            
        }
    }
}
