﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class PostgresqlBlinkayEntityRepository : IntEntityRepository<BlinkayEntity>, IPostgresqlBlinkayEntityRepository
    {
        public PostgresqlBlinkayEntityRepository(IPostgresqlBlinkayUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
