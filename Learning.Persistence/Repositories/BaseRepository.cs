﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Learning.Persistence.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public IUnitOfWork UnitOfWork { get; }

        protected DbSet<T> Set { get; set; }        

        public BaseRepository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            Set = unitOfWork.DbContext.Set<T>();            
        }        

        public T Create(T instance)
        {
            instance.CreateDate = DateTime.Now;
            instance.UpdateDate = DateTime.Now;
            return Set.Add(instance).Entity;
        }

        public T Update(T instance)
        {
            instance.UpdateDate = DateTime.Now;
            return Set.Update(instance).Entity;
        }

        public void Delete(T instance)
        { 
            Set.Remove(instance);
        }

        public async Task<int> Count(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null) 
        {
            IQueryable<T> query = GetQuery(filters, includes);

            return await query.CountAsync();
        }

        public async Task<ICollection<T>> Get(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null,
                                              int limit = 0, int offset = 0, string orderByPropertyName = null,
                                              bool ascendingOrderBy = true)
        {
            IQueryable<T> query = GetQuery(filters, includes, limit, offset, orderByPropertyName, ascendingOrderBy);

            return await query.ToListAsync();
        }
        
        public async Task<IPagedResult<T>> GetPaged(ICollection<Expression<Func<T, bool>>> filters, int limit = 0, int offset = 0, string orderByPropertyName = "CreateDate",
                                  bool ascendingOrderBy = true, bool includeTotalCount = true, ICollection<string> includes = null)
        {
            IQueryable<T> query = GetQuery(filters, includes, limit, offset, orderByPropertyName, ascendingOrderBy);
            List<T> items = await query.ToListAsync<T>();

            int totalCount = 0;
            if (includeTotalCount)
            {
                query = GetQuery(filters, includes, 0, 0, orderByPropertyName, ascendingOrderBy);
                totalCount = await query.CountAsync();
            }
           
            PagedResult<T> pagedResult = new PagedResult<T>(items, totalCount);

            return pagedResult;
        }

        public async Task<ICollection<T>> Get(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null,
                                 string orderByPropertyName = "CreateDate", bool ascendingOrderBy = true)
        {
            IQueryable<T> query = GetQuery(filters, includes, 0, 0, orderByPropertyName, ascendingOrderBy);

            return await query.ToListAsync(); ;
        }

        private IQueryable<T> GetQuery(ICollection<Expression<Func<T, bool>>> filters, ICollection<string> includes = null, 
                                              int limit = 0, int offset = 0, string orderByPropertyName = null, 
                                              bool ascendingOrderBy = true)
        {
            IQueryable<T> query = Set.AsQueryable<T>();

            if (includes != null) 
            {
                foreach (string property in includes)
                {
                    query = query.Include(property);
                }
            }

            if (filters != null)
            {
                foreach (Expression<Func<T, bool>> filter in filters)
                {
                    query = query.Where(filter);
                }
            }

           query = RepositoryHelper<T>.ApplyOrder(query, orderByPropertyName, ascendingOrderBy);

            if (offset > 0)
            {
                query = query.Skip(offset);
            }

            if (limit > 0)
            {
                query = query.Take(limit);
            }

            return query;
        }              
    }
}
