﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;
using System;
using System.Threading.Tasks;

namespace Learning.Persistence.Repositories
{
    public class GuidEntityRepository<T> : BaseRepository<T>, IGuidEntityRepository<T> where T : BaseGuidEntity
    {
        public GuidEntityRepository(ILearningUnitOfWork unitOfWork): base(unitOfWork)
        {
        }

        public async Task Delete(Guid id)
        {
            Set.Remove(await GetById(id));
        }

        public async Task<T> GetById(Guid Id)
        {
            return await Set.FindAsync(Id);
        }
    }
}
