﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence.UnitOfWork;

namespace Learning.Persistence.Repositories
{
    public class CustomerRepository: GuidEntityRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ILearningUnitOfWork unitOfWork): base(unitOfWork)
        {            
        }
    }
}
