﻿using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using System.Collections.Generic;

namespace Learning.Persistence
{
    public class PagedResult<T> : IPagedResult<T> 
    {
        public List<T> Items { get; set; }
        public int TotalCount { get; set; }

        public PagedResult(List<T> items, int totalCount)
        {
            this.Items = items;
            this.TotalCount = totalCount;
        }
    }
}
