﻿using Learning.Domain.Entities;
using Learning.Common;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Learning.Persistence
{
    public static class RepositoryHelper<T> where T : BaseEntity
    {
        public static IQueryable<T> ApplyOrder(IQueryable<T> query, string orderByPropertyName, bool ascendingOrderBy)
        {
            if (!string.IsNullOrWhiteSpace(orderByPropertyName))
            {
                if (ascendingOrderBy)
                {
                    query = query.OrderBy(ToLambda(orderByPropertyName));
                }
                else
                {
                    query = query.OrderByDescending(ToLambda(orderByPropertyName));
                }
            }
            return query;
        }

        private static Expression<Func<T, object>> ToLambda(string propertyName)
        {
            if (!PropertyExists(propertyName)) 
                throw new LearningException($"La propiedad {propertyName} no existe en {typeof(T).Name}");

            var parameter = Expression.Parameter(typeof(T));
            var property = Expression.Property(parameter, propertyName);
            var propAsObject = Expression.Convert(property, typeof(object));

            return Expression.Lambda<Func<T, object>>(propAsObject, parameter);
        }

        private static bool PropertyExists(string propertyName)
        {
            var property = typeof(T).GetProperties().FirstOrDefault(p => p.Name.ToUpperInvariant() == propertyName.ToUpperInvariant());
            return property == null ? false : true;
        }
    }
}
