﻿using System;

namespace Learning.Common
{
    public class LearningException : Exception
    {
        public LearningException(string message) : base(message) { }
    }
}
