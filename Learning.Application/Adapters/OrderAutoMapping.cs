﻿using AutoMapper;
using Learning.Application.Dtos.Order;
using Learning.Domain.Entities;

namespace Learning.Application.Adapters
{
    public class OrderAutoMapping : Profile
    {
        public OrderAutoMapping()
        {
            CreateMap<Order, OrderListDto>();
            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>(); 
        }
    }
}
