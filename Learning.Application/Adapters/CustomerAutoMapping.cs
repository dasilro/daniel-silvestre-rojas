﻿using AutoMapper;
using Learning.Application.Dtos.Customer;
using Learning.Domain.Entities;

namespace Learning.Application.Adapters
{
    public class CustomerAutoMapping : Profile
    {
        public CustomerAutoMapping()
        {
            CreateMap<Customer, CustomerDto>(); // means you want to map from Customer to CustomerDto
            CreateMap<Customer, CustomerListDto>();
            CreateMap<CustomerDto, Customer>(); 
        }
    }
}
