﻿using AutoMapper;
using Learning.Application.Dtos.Product;
using Learning.Domain.Entities;

namespace Learning.Application.Adapters
{
    public class ProductAutoMapping : Profile
    {
        public ProductAutoMapping()
        {
            CreateMap<Product, ProductListDto>();
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>(); 
        }
    }
}
