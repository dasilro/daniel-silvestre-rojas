﻿using AutoMapper;
using Learning.Application.Dtos.OrderLine;
using Learning.Domain.Entities;

namespace Learning.Application.Adapters
{
    public class OrderLineAutoMapping : Profile
    {
        public OrderLineAutoMapping()
        {
            CreateMap<OrderLine, OrderLineListDto>();
            CreateMap<OrderLine, OrderLineDto>();
            CreateMap<OrderLineDto, OrderLine>(); 
        }
    }
}
