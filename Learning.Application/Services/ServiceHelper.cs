﻿using Learning.Application.Dtos;
using Learning.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Learning.Application.Services
{
    public static class ServiceHelper<T> where T : BaseGuidEntity
    {

        public static List<Expression<Func<T, bool>>> Get(BaseGuidEntityQueryDto queryDto = null) 
        {
            List<Expression<Func<T, bool>>> filters = new List<Expression<Func<T, bool>>>();

            if (queryDto != null)
            {
                if (queryDto.Id.HasValue)
                {
                    filters.Add(x => x.Id == queryDto.Id);
                }

                if (queryDto.FromCreateDate.HasValue)
                {
                    filters.Add(x => x.CreateDate >= queryDto.FromCreateDate);
                }

                if (queryDto.ToCreateDate.HasValue)
                {
                    filters.Add(x => x.CreateDate <= queryDto.ToCreateDate);
                }

                if (queryDto.FromUpdateDate.HasValue)
                {
                    filters.Add(x => x.UpdateDate >= queryDto.FromUpdateDate);
                }

                if (queryDto.ToUpdateDate.HasValue)
                {
                    filters.Add(x => x.UpdateDate <= queryDto.ToUpdateDate);
                }
            }

            return filters;
        }
       
    }
}
