﻿using AutoMapper;
using Learning.Application.Dtos.Order;
using Learning.Application.Dtos.OrderLine;
using Learning.Common;
using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public class OrderService : IOrderService
    {
        private IOrderRepository orderRepository { get; set; }
        private ICustomerRepository customerRepository { get; set; }
        private IProductRepository productRepository { get; set; }
        private IMapper mapper { get; set; }

        public OrderService(IOrderRepository orderRepository, ICustomerRepository customerRepository, 
                            IProductRepository productRepository, IMapper mapper)
        {
            this.orderRepository = orderRepository;
            this.customerRepository = customerRepository;
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public async Task<OrderDto> Create(OrderDto orderDto)
        {
            await ValidateOrderDto(orderDto);

            Order order = mapper.Map<Order>(orderDto);

            order.Id = Guid.NewGuid();
            order.CreateDate = DateTime.Now;
            order.UpdateDate = DateTime.Now;
            OrderSetTotals(order);

            orderRepository.Create(order);
            await orderRepository.UnitOfWork.SaveChanges();

            return mapper.Map<OrderDto>(order);
        }        

        public async Task<OrderDto> Update(OrderDto orderDto)
        {
            await ValidateOrderDto(orderDto);

            if (!orderDto.Id.HasValue) throw new LearningException("Es necesio un Id de pedido a modificar.");

            Order order = mapper.Map<Order>(orderDto);

            OrderSetTotals(order);
            order.UpdateDate = DateTime.Now;

            orderRepository.Update(order);
            await orderRepository.UnitOfWork.SaveChanges();

            return mapper.Map<OrderDto>(order);
        }

        public async Task<OrderDto> GetById(Guid id)
        {
            return mapper.Map<OrderDto>(await this.orderRepository.GetById(id));
        }

        public async Task<IPagedResult<OrderListDto>> Get(OrderQueryDto queryDto = null)
        {
            List<Expression<Func<Order, bool>>> filters = ServiceHelper<Order>.Get(queryDto);

            if (queryDto != null)
            {
                if (queryDto.CustomerId.HasValue)
                {
                    filters.Add(x => x.CustomerId == queryDto.CustomerId);
                }

                if (queryDto.FromDate.HasValue)
                {
                    filters.Add(x => x.Date >= queryDto.FromDate);
                }

                if (queryDto.ToDate.HasValue)
                {
                    filters.Add(x => x.Date <= queryDto.ToDate);
                }

                if (queryDto.FromTotal.HasValue)
                {
                    filters.Add(x => x.Total >= queryDto.FromTotal);
                }

                if (queryDto.ToTotal.HasValue)
                {
                    filters.Add(x => x.Total <= queryDto.ToTotal);
                }

                if (!string.IsNullOrWhiteSpace(queryDto.SerialNumber))
                {
                    filters.Add(x => x.SerialNumber.Contains(queryDto.SerialNumber));
                }

            }

            IPagedResult<Order> orders = await orderRepository.GetPaged(filters, queryDto.Limit??0, queryDto.OffSet??0, 
                                                                                queryDto.OrderByProperty, queryDto.OrderByAsc??true, 
                                                                                true);
            IPagedResult<OrderListDto> result = new PagedResult<OrderListDto>(mapper.Map<List<OrderListDto>>(orders.Items)
                                                                      , orders.TotalCount);

            return result;
        }

        public async Task Delete(Guid id)
        {
            await this.orderRepository.Delete(id);
            await orderRepository.UnitOfWork.SaveChanges();
        }

        private async Task ValidateOrderLineDto(OrderLineDto orderLineDto)
        {
            if (orderLineDto == null) throw new LearningException("La línea de pedido es nula.");

            if (orderLineDto.DiscountAmount < 0) throw new LearningException("El importe de descuento no puede ser negativo.");

            if (orderLineDto.DiscountPercentage < 0) throw new LearningException("El porcentaje de descuento no puede ser negativo.");

            if (orderLineDto.ProductQuantity < 0) throw new LearningException("La cantidad de unidades no puede ser negativa.");

            if (orderLineDto.TaxAmount < 0) throw new LearningException("El importe de impuestos no puede ser negativo.");

            if (orderLineDto.TaxPercentage < 0) throw new LearningException("El porcentaje de impuestos no puede ser negativo.");

            if (orderLineDto.UnitaryPrice < 0) throw new LearningException("La cantidad de unidades no puede ser negativa.");

            if (orderLineDto.WithoutTaxesTotal < 0) throw new LearningException("El total sin impuestos no puede ser negativo.");

            Product product = await productRepository.GetById(orderLineDto.ProductId);
            if (product == null) throw new LearningException("El identificador de producto es requerido.");
        }

        private async Task ValidateOrderDto(OrderDto orderDto)
        {
            if (orderDto == null) throw new LearningException("El pedido es nulo.");

            if (orderDto.Date == System.DateTime.MinValue) throw new LearningException("La fecha del pedido es un campo obligatorio.");

            if (orderDto.Total < 0) throw new LearningException("El total del pedido no puede ser negativo.");

            if (!orderDto.OrderLines.Any()) throw new LearningException("El pedido debe tener algún producto.");

            Customer customer = await customerRepository.GetById(orderDto.CustomerId);
            if (customer == null) throw new LearningException("El cliente es un campo obligatorio.");

            foreach (OrderLineDto orderLineDto in orderDto.OrderLines)
            {
                await ValidateOrderLineDto(orderLineDto);
            }
        }

        private void OrderLineSetTotals(OrderLine orderLine)
        {
            decimal withoutDtoAndTaxesTotal = Math.Round(orderLine.UnitaryPrice * orderLine.ProductQuantity, 2);
            orderLine.DiscountAmount = orderLine.DiscountPercentage >= 100 ? withoutDtoAndTaxesTotal : Math.Round(withoutDtoAndTaxesTotal * orderLine.DiscountPercentage / 100, 2);
            orderLine.WithoutTaxesTotal = withoutDtoAndTaxesTotal - orderLine.DiscountAmount;
            orderLine.TaxAmount = Math.Round((orderLine.WithoutTaxesTotal * (orderLine.TaxPercentage)) / 100, 2);
            orderLine.Total = orderLine.WithoutTaxesTotal + orderLine.TaxAmount;
        }

        private void OrderSetTotals(Order order)
        {
            order.OrderLines.ToList().ForEach(o => OrderLineSetTotals(o));

            order.WithoutTaxesTotal = order.OrderLines.Sum(l => l.WithoutTaxesTotal);
            order.TaxAmount = order.OrderLines.Sum(l => l.TaxAmount);
            order.Total = order.OrderLines.Sum(l => l.Total);
        }

    }
}
