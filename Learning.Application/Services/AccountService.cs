﻿using Learning.Application.Dtos.Account;
using Learning.Application.Services.Interfaces;
using Learning.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IConfiguration configuration;

        public AccountService(UserManager<ApplicationUser> userManager, IConfiguration configuration) 
        {
            this.userManager = userManager;
            this.configuration = configuration;
        }

        public async Task<string> GetTokenJwt(string email, string password)
        {
            // Verificamos credenciales con Identity
            var user = await userManager.FindByEmailAsync(email);

            if (user is null || !await userManager.CheckPasswordAsync(user, password))
            {
                throw new UnauthorizedAccessException();
            }

            var roles = await userManager.GetRolesAsync(user);

            // Generamos un token según los claims
            var claims = new List<Claim>
            {
                new Claim("id", user.Id),
                new Claim("name", user.UserName),
                new Claim("email", $"{user.Email} ")
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddMinutes(720),
                SigningCredentials = credentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<IdentityResult> Register(RegisterDto registerDto)
        {
            var newUser = new ApplicationUser()
            {
                Id = System.Guid.NewGuid().ToString(),
                Email = registerDto.Email,
                UserName = registerDto.Name,
                NormalizedUserName = $"{registerDto.Name} {registerDto.LastName}"
            };

            return await userManager.CreateAsync(newUser, registerDto.Password);
        }
    }
}
