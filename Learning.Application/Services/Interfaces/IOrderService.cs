﻿using Learning.Application.Dtos.Order;
using Learning.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public interface IOrderService
    {
        Task<OrderDto> GetById(Guid id);
        Task<IPagedResult<OrderListDto>> Get(OrderQueryDto queryDto = null);
        Task<OrderDto> Create(OrderDto dto);
        Task<OrderDto> Update(OrderDto dto);
        Task Delete(Guid id);
    }
}
