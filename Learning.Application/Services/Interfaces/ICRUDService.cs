﻿using Learning.Application.Dtos;
using Learning.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public interface ICRUDService<T, Q, D> where T : BaseEntity where Q : BaseEntityQueryDto where D : BaseEntityDto 
    {
        Task<T> GetById(Guid Id);
        Task<ICollection<T>> Get(Q queryDto = null);
        Task<T> Create(D dto);
        Task<T> Update(D dto);
        Task Delete(Guid Id);
    }
}
