﻿using Learning.Application.Dtos.Customer;
using Learning.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public interface ICustomerService
    {
        Task<CustomerDto> GetById(Guid id);
        Task<IPagedResult<CustomerListDto>> Get(CustomerQueryDto queryDto = null);
        Task<CustomerDto> Create(CustomerDto dto);
        Task<CustomerDto> Update(CustomerDto dto);
        Task Delete(Guid id);
    }
}
