﻿using Learning.Application.Dtos.Account;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Learning.Application.Services.Interfaces
{
    public interface IAccountService
    {
        public Task<IdentityResult> Register(RegisterDto registerDto);
        public Task<string> GetTokenJwt(string email, string password);
    }
}