﻿using Learning.Application.Dtos.Product;
using Learning.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetById(Guid id);
        Task<IPagedResult<ProductListDto>> Get(ProductQueryDto queryDto = null);
        Task<ProductDto> Create(ProductDto dto);
        Task<ProductDto> Update(ProductDto dto);
        Task Delete(Guid id);
    }
}
