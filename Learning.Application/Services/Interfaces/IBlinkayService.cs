﻿using System.Threading.Tasks;

namespace Learning.Application.Services.Interfaces
{
    public interface IBlinkayService
    {
        Task<double> MySQLInsertion(int numRegistries, int numThreads);
        Task<double> MySQLSelectPlusUpdate(int numRegistries, int numThreads);
        Task<double> MySQLSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads);

        Task<double> PGInsertion(int numRegistries, int numThreads);
        Task<double> PGSelectPlusUpdate(int numRegistries, int numThreads);
        Task<double> PGSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads);
    }
}
