﻿using Learning.Application.Services.Interfaces;
using Learning.Common;
using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public class BlinkayService: IBlinkayService
    {
        private readonly IServiceProvider serviceProvider;

        public BlinkayService(IServiceProvider serviceProvider) 
        {
            this.serviceProvider = serviceProvider;
        }

        private void ValidateMoreThanZero(int numRegistries, int numThreads) 
        { 
            if (numRegistries < 1) 
            {
                throw new LearningException("El número de registros debe ser un número entero mayor a cero.");
            }

            if (numThreads < 1)
            {
                throw new LearningException("El número de hilos debe ser un número entero mayor a cero.");
            }
        }

        #region Insertion

        public async Task<double> MySQLInsertion(int numRegistries, int numThreads)
        {            
            return await Insertion<IMySqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        public async Task<double> PGInsertion(int numRegistries, int numThreads)
        {
            return await Insertion<IPostgresqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        private async Task<double> Insertion<T>(int numRegistries, int numThreads) where T : IIntEntityRepository<BlinkayEntity>
        {
            var startTimer = DateTime.Now;

            ValidateMoreThanZero(numRegistries, numThreads);

            var tasks = new List<Task>();
            for (int threadCounter = 0; threadCounter < numThreads; threadCounter++)
            {
                // Execute a TPL task for each thread.
                var task = Task.Run(async () =>
                {
                    await InsertRegistries<T>(numRegistries);
                    return;
                });

                tasks.Add(task);
            }

            // awaits the execution of all tasks.
            await Task.WhenAll(tasks);

            var endTimer = DateTime.Now;
            return (endTimer - startTimer).TotalMilliseconds;
        }

        private async Task InsertRegistries<T>(int numRegistries) where T : IIntEntityRepository<BlinkayEntity>
        {
            // To allow concurrent work each task will use its own DbContext.
            // The IoC repository configuration for IMySqlBlinkayUnitOfWork, IMySqlBlinkayEntityRepository and MySqlBlinkayDbContext
            // is Transient, it ensures new instances.
            var repository = serviceProvider.GetService<T>();
            for (int iterationCounter = 0; iterationCounter < numRegistries; iterationCounter++)
            {
                await InsertSingleRegistryIteration(repository);
                Console.WriteLine($"Insert Threat {Thread.CurrentThread.ManagedThreadId} iteration {iterationCounter} ");
            }
        }

        private async Task InsertSingleRegistryIteration(IIntEntityRepository<BlinkayEntity> repository)
        {
            var blinkayEntity = new BlinkayEntity() { Name = Guid.NewGuid().ToString() };
            var transaction = await repository.UnitOfWork.BeginTransaction();
            try
            {
                repository.Create(blinkayEntity);
                await repository.UnitOfWork.SaveChanges();
                await repository.UnitOfWork.CommitTransaction(transaction);
            }
            catch (Exception)
            {
                repository.UnitOfWork.CancelChanges();
                await repository.UnitOfWork.RollbackTransaction(transaction);
            }
        }

        #endregion

        #region SelectPlusUpdate

        public async Task<double> MySQLSelectPlusUpdate(int numRegistries, int numThreads) 
        {
            return await SelectPlusUpdate<IMySqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        public async Task<double> PGSelectPlusUpdate(int numRegistries, int numThreads)
        {
            return await SelectPlusUpdate<IPostgresqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        private async Task<double> SelectPlusUpdate<T>(int numRegistries, int numThreads) where T : IIntEntityRepository<BlinkayEntity>
        {
            var startTimer = DateTime.Now;

            ValidateMoreThanZero(numRegistries, numThreads);

            var tasks = new List<Task>();
            for (int threadCounter = 0; threadCounter < numThreads; threadCounter++)
            {
                // Execute a TPL task for each thread.
                var task = Task.Run(async () =>
                {
                    await SelectPlusUpdateRegistries<T>(numRegistries);
                    return;
                });

                tasks.Add(task);
            }

            // awaits the execution of all tasks.
            await Task.WhenAll(tasks);

            var endTimer = DateTime.Now;
            return (endTimer - startTimer).TotalMilliseconds;
        }

        private async Task SelectPlusUpdateRegistries<T>(int numRegistries) where T : IIntEntityRepository<BlinkayEntity>
        {
            var repository = serviceProvider.GetService<T>();
            var minValue = (await repository.GetPaged(null, 1, 0, "Id", true, false))?.Items[0]?.Id ?? 0;
            var maxValue = (await repository.GetPaged(null, 1, 0, "Id", false, false))?.Items[0]?.Id ?? 0;

            Random random = new Random();
            for (int iterationCounter = 0; iterationCounter < numRegistries; iterationCounter++)
            {
                var randomId = random.Next(minValue, maxValue);
                await SelectPlusUpdateIteration(repository, randomId);
                Console.WriteLine($"Threat {Thread.CurrentThread.ManagedThreadId} iteration {iterationCounter} ");
            }
        }

        private async Task SelectPlusUpdateIteration(IIntEntityRepository<BlinkayEntity> repository, int registryId) 
        {            
            var blinkayEntity = await repository.GetById(registryId);
            if (blinkayEntity == null) return;

            blinkayEntity.Name = Guid.NewGuid().ToString();
            var transaction = await repository.UnitOfWork.BeginTransaction();
            try
            {
                repository.Update(blinkayEntity);
                await repository.UnitOfWork.SaveChanges();
                await repository.UnitOfWork.CommitTransaction(transaction);
            }
            catch (Exception)
            {
                repository.UnitOfWork.CancelChanges();
                await repository.UnitOfWork.RollbackTransaction(transaction);
            }
        }

        #endregion

        #region SelectPlusUpdatePlusInsertion

        public async Task<double> MySQLSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads)
        {
            return await SelectPlusUpdatePlusInsertion<IMySqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        public async Task<double> PGSelectPlusUpdatePlusInsertion(int numRegistries, int numThreads)
        {
            return await SelectPlusUpdatePlusInsertion<IPostgresqlBlinkayEntityRepository>(numRegistries, numThreads);
        }

        private async Task<double> SelectPlusUpdatePlusInsertion<T>(int numRegistries, int numThreads) where T : IIntEntityRepository<BlinkayEntity>
        {
            var startTimer = DateTime.Now;

            await Insertion<T>(numRegistries, numThreads);
            await SelectPlusUpdate<T>(numRegistries, numThreads);

            var endTimer = DateTime.Now;
            return (endTimer - startTimer).TotalMilliseconds;
        }

        #endregion
    }
}
