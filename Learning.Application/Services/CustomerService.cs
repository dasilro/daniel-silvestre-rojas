﻿using AutoMapper;
using Learning.Application.Dtos.Customer;
using Learning.Common;
using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository customerRepository { get; set; }
        private IMapper mapper { get; set; }

        public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
        {
            this.customerRepository = customerRepository;
            this.mapper = mapper;
        }

        public async Task<CustomerDto> Create(CustomerDto customerDto)
        {
            if (string.IsNullOrEmpty(customerDto.Name)) throw new LearningException("El nombre es un campo requerido");

            Customer customer = mapper.Map<Customer>(customerDto);

            customer.Id = Guid.NewGuid();
            customer.CreateDate = DateTime.Now;
            customer.UpdateDate = DateTime.Now;

            customerRepository.Create(customer);
            await customerRepository.UnitOfWork.SaveChanges();

            return mapper.Map<CustomerDto>(customer);
        }

        public async Task<CustomerDto> Update(CustomerDto customerDto)
        {
            if (string.IsNullOrEmpty(customerDto.Name)) throw new LearningException("El nombre es un campo requerido");

            Customer customer = mapper.Map<Customer>(customerDto);

            customer.UpdateDate = DateTime.Now;

            customerRepository.Update(customer);
            await customerRepository.UnitOfWork.SaveChanges();

            return mapper.Map<CustomerDto>(customer);
        }

        public async Task<CustomerDto> GetById(Guid id)
        {
            return mapper.Map<CustomerDto>(await this.customerRepository.GetById(id));
        }

        public async Task<IPagedResult<CustomerListDto>> Get(CustomerQueryDto queryDto = null)
        {            
            List<Expression<Func<Customer, bool>>> filters = ServiceHelper<Customer>.Get(queryDto);

            if (queryDto != null)
            {
                if (!string.IsNullOrWhiteSpace(queryDto.Name))
                {
                    filters.Add(x => x.Name.Contains(queryDto.Name));
                }
            }

            IPagedResult<Customer> customers = await customerRepository.GetPaged(filters, queryDto.Limit??0, queryDto.OffSet??0, 
                                                                                queryDto.OrderByProperty, queryDto.OrderByAsc??true, 
                                                                                true);

            IPagedResult<CustomerListDto> result = new PagedResult<CustomerListDto>(mapper.Map<List<CustomerListDto>>(customers.Items)
                                                                      , customers.TotalCount);
            return result;

        }

        public async Task Delete(Guid id)
        {
            await this.customerRepository.Delete(id);
            await customerRepository.UnitOfWork.SaveChanges();
        }
    }
}
