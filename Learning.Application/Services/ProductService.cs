﻿using AutoMapper;
using Learning.Application.Dtos.Product;
using Learning.Common;
using Learning.Domain.Entities;
using Learning.Domain.Interfaces;
using Learning.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Learning.Application.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository productRepository { get; set; }
        private IMapper mapper { get; set; }

        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public async Task<ProductDto> Create(ProductDto productDto)
        {
            if (string.IsNullOrEmpty(productDto.Name)) throw new LearningException("El nombre es un campo requerido");
            if (productDto.UnitaryPrice<0) throw new LearningException("El precio no puede ser inferior a 0");

            Product product = mapper.Map<Product>(productDto);

            product.Id = Guid.NewGuid();
            product.CreateDate = DateTime.Now;
            product.UpdateDate = DateTime.Now;

            productRepository.Create(product);
            await productRepository.UnitOfWork.SaveChanges();

            return mapper.Map<ProductDto>(product);
        }

        public async Task<ProductDto> Update(ProductDto productDto)
        {
            if (string.IsNullOrEmpty(productDto.Name)) throw new LearningException("El nombre es un campo requerido");
            if (productDto.UnitaryPrice < 0) throw new LearningException("El precio no puede ser inferior a 0");

            Product product = mapper.Map<Product>(productDto);

            product.UpdateDate = DateTime.Now;

            productRepository.Update(product);
            await productRepository.UnitOfWork.SaveChanges();

            return mapper.Map<ProductDto>(product);
        }

        public async Task<ProductDto> GetById(Guid id)
        {
            return mapper.Map<ProductDto>(await this.productRepository.GetById(id));
        }

        public async Task<IPagedResult<ProductListDto>> Get(ProductQueryDto queryDto = null)
        {
            

            List<Expression<Func<Product, bool>>> filters = ServiceHelper<Product>.Get(queryDto);

            if (queryDto != null)
            {
                if (!string.IsNullOrWhiteSpace(queryDto.Name))
                {
                    filters.Add(x => x.Name.Contains(queryDto.Name));
                }

                if (queryDto.FromUnitaryPrice.HasValue)
                {
                    filters.Add(x => x.UnitaryPrice >= queryDto.FromUnitaryPrice);
                }

                if (queryDto.ToUnitaryPrice.HasValue)
                {
                    filters.Add(x => x.UnitaryPrice <= queryDto.ToUnitaryPrice);
                }
            }

            IPagedResult<Product> products = await productRepository.GetPaged(filters, queryDto.Limit??0, queryDto.OffSet??0, 
                                                                                queryDto.OrderByProperty, queryDto.OrderByAsc??true, 
                                                                                true);

            IPagedResult<ProductListDto> result = new PagedResult<ProductListDto>(mapper.Map<List<ProductListDto>>(products.Items)
                                                                                  , products.TotalCount);
            return result;
        }

        public async Task Delete(Guid id)
        {
            await this.productRepository.Delete(id);
            await productRepository.UnitOfWork.SaveChanges();
        }
    }
}
