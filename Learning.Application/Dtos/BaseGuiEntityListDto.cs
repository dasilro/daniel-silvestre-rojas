﻿using System;

namespace Learning.Application.Dtos
{
    public class BaseGuidEntityListDto
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
