﻿using System;

namespace Learning.Application.Dtos
{
    public class BaseGuidEntityDto
    {
        public Guid? Id { get; set; }
    }
}
