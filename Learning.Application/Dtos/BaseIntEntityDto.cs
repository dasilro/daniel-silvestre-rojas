﻿using System;

namespace Learning.Application.Dtos
{
    public class BaseIntEntityDto
    {
        public int? Id { get; set; }
    }
}
