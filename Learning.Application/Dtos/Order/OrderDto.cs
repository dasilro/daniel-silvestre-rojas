﻿using Learning.Application.Dtos.Customer;
using Learning.Application.Dtos.OrderLine;
using System;
using System.Collections.Generic;

namespace Learning.Application.Dtos.Order
{
    public class OrderDto: BaseGuidEntityDto
    {
        public string SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public Guid CustomerId { get; set; }
        public CustomerListDto Customer { get; set; }
        public decimal WithoutTaxesTotal { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public virtual ICollection<OrderLineDto> OrderLines { get; set; }
    }
}
