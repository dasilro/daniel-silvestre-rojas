﻿using System;

namespace Learning.Application.Dtos.Order
{
    public class OrderListDto: BaseGuidEntityListDto
    {
        public string SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }
        public decimal WithoutTaxesTotal { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
    }
}
