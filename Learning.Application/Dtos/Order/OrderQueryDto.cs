﻿using System;

namespace Learning.Application.Dtos.Order
{
    public class OrderQueryDto: BaseGuidEntityQueryDto
    {
        public string SerialNumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Guid? CustomerId { get; set; }
        public decimal? FromTotal { get; set; }
        public decimal? ToTotal { get; set; }
    }
}
