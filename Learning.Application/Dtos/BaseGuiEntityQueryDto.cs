﻿using System;

namespace Learning.Application.Dtos
{
    public class BaseGuidEntityQueryDto
    {
        public Guid? Id { get; set; }
        public DateTime? FromCreateDate { get; set; }
        public DateTime? ToCreateDate { get; set; }
        public DateTime? FromUpdateDate { get; set; }
        public DateTime? ToUpdateDate { get; set; }

        public string OrderByProperty { get; set; }
        public bool? OrderByAsc { get; set; }
        public int? Limit { get; set; }
        public int? OffSet { get; set; }
    }
}
