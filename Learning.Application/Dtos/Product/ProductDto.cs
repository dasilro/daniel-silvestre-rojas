﻿using System;

namespace Learning.Application.Dtos.Product
{
    public class ProductDto: BaseGuidEntityDto
    {
        public string Name { get; set; }
        public decimal UnitaryPrice { get; set; }
    }
}
