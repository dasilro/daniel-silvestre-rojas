﻿namespace Learning.Application.Dtos.Product
{
    public class ProductQueryDto : BaseGuidEntityQueryDto
    {
        public string Name { get; set; }
        public decimal? FromUnitaryPrice { get; set; }
        public decimal? ToUnitaryPrice { get; set; }
    }
}
