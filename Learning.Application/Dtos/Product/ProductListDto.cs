﻿namespace Learning.Application.Dtos.Product
{
    public class ProductListDto : BaseGuidEntityListDto
    {
        public string Name { get; set; }
        public decimal UnitaryPrice { get; set; }
    }
}
