﻿using System;

namespace Learning.Application.Dtos.Customer
{
    public class CustomerListDto: BaseGuidEntityListDto
    {        
        public string Name { get; set; }
    }
}
