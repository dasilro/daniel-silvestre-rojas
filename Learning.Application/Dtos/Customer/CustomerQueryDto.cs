﻿namespace Learning.Application.Dtos.Customer
{
    public class CustomerQueryDto: BaseGuidEntityQueryDto
    {
        public string Name { get; set; }
    }
}
