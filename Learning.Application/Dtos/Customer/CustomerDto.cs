﻿using System;

namespace Learning.Application.Dtos.Customer
{
    public class CustomerDto: BaseGuidEntityDto
    {
        public string Name { get; set; }
    }
}
