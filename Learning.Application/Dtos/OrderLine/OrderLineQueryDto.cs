﻿using System;

namespace Learning.Application.Dtos.OrderLine
{
    public class OrderLineQueryDto: BaseGuidEntityQueryDto
    {
        public Guid OrderId { get; set; }
    }
}
